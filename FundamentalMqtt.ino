#define SerialAT Serial3

#define TINY_GSM_MODEM_SIM808

#include <TinyGsmClient.h>
#include <PubSubClient.h>

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);

/*
   GPRS
   0. Turn on the SIM
   1. Find the baudrate SIM
   2. Restart SIM
   3. Get Modem Info
   4. Connect to GSM Network
   5. Check connection GSM
   6. Connect to GPRS
   7. Check connection GPRS
   8. Get CCID for id MQTT
   9. Get Imei for alternative id MQTT
   10. Get Operator
   11. Get Local IP Address

   MQTT
   0. Connect to MQTT
   1. Check MQTT Connection
   2. Publish Message
   3. Subscribe Topic
*/
boolean isReadyMqtt = false;
boolean isReadyGprs = false;

uint32_t rateSim = 0;
String ccidSim = "";

unsigned long tmIntervlMqtt = 0;

void setup() {
  Serial.begin(9600);
}

void SendData(int interval) {
  if (millis() > tmIntervlMqtt + (1000 * interval)) {
    tmIntervlMqtt = millis();

    mqtt.publish("pub", "coba");
    mqtt.subscribe("sub");
  }
}

void loop() {
  if (mqtt.connected()) {
    SendData(2);
  }
  else
    ConnectingToGprs("tracker.widya.ai",
                     1883,
                     "internet",
                     "",
                     "");
  mqtt.loop();
}
