void TurnOnSim() {
  const int keySim = 2;
  const int res = 4;
  pinMode(res, OUTPUT);
  digitalWrite(res, LOW);
  pinMode(keySim, OUTPUT);
  digitalWrite(keySim, HIGH);
  delay(1000);
  digitalWrite(keySim, LOW);
  Serial.println("[Sim-808] Turned On");
}

boolean ConnectingToGprs(String broker,
                         int port,
                         String apn,
                         String user,
                         String pass) {

  for (int i = 0; i < 15; i++) {
    switch (i) {
      case (0):
        delay(1000);
        Serial.println("[Sim-808] Recovery");
      case (1): //Key SIM Turn On
        TurnOnSim();
        break;
      case (2): // Search the baud rate of SIM
        if (!rateSim) {
          rateSim = TinyGsmAutoBaud(SerialAT);
        }
        break;
      case (3):
        if (modem.restart()) {
          Serial.println("[Sim-808] Series : "
                         + String(modem.getModemInfo()));
        } else i = 0;
        break;
      case (4): // Connect to GSM Network
        if (!modem.waitForNetwork()) {
          delay(5000);
          Serial.println(F("[Sim-808] Network is not connected"));
          i--;
        }
        else {
          if (modem.isNetworkConnected()) {
            Serial.println(F("[Sim-808] Network is connected"));
          } else i--;
        }
        break;
      case (5): // Connect to Internet/GPRS Network
        if (!modem.gprsConnect(apn.c_str(),
                               user.c_str(),
                               pass.c_str())) {
          Serial.println(F("[Sim-808] GPRS is not connected"));
          i--;
        } else {
          if (modem.isGprsConnected()) { // Connect to GPRS

            IPAddress ipSim = modem.localIP(); // it dinamics
            Serial.println("[Sim-808] IP Address : " +
                           String(ipSim));
            delay(200);
            String operatorSim = modem.getOperator(); //
            Serial.println("[Sim-808] Operator : " +
                           operatorSim);

            delay(200);
            ccidSim = modem.getSimCCID();
            Serial.println("[Sim-808] CCID : " +
                           ccidSim);

            delay(200);
            String imeiSim = modem.getIMEI();
            Serial.println("[Sim-808] IMEI : " +
                           imeiSim);
          } else i--;
          break;
        }
      case (6):
        mqtt.setServer(broker.c_str(), port);
        mqtt.setCallback(mqttCallback);
        Serial.println("Connecting to " + broker);
        break;
      case (7):
        if (mqtt.connect(ccidSim.c_str())) {
          Serial.println("Connected to " + broker);
        } else i = 0;
        return mqtt.connected();
        break;
    }
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int len) {
  String strPayload = "",
         strTopic = "";

  for (int i = 0; i < len; i++) {
    strPayload += (char)payload[i];
    strTopic   += (char)topic[i];
  }

  Serial.print(F("[Sub] Topic : "));
  Serial.print(F(strTopic.c_str()));
  Serial.print(F(" Payload :"));
  Serial.println(F(strPayload.c_str()));
}
